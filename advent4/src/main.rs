use std::collections::HashSet;

fn valid_password(pass: &Vec<u8>) -> bool {
    // Is a 6 digit number
    if !pass.len() == 6 {
        return false;
    }
    // digits never decrease
    for i in 0..5 {
        if pass[i + 1] < pass[i] {
            return false;
        }
    }

    // Two adjacent digits are the same
    for i in 0..5 {
        if pass[i + 1] == pass[i] {
            return true;
        }
    }

    return false;
}

fn next_password(mut pass: Vec<u8>) -> Vec<u8> {
    // Add 1 to password, starting at end
    pass[5] += 1;
    let mut overflow = pass[5] >= 10;
    let mut index = 5;
    while overflow && index > 0 {
        pass[index] = 0;
        pass[index - 1] += 1;
        overflow = pass[index - 1] >= 10;
        index -= 1;
    }
    pass
}

fn lt(pass: &Vec<u8>, limit: &Vec<u8>) -> bool {
    // highest order to lowest order comparison
    for i in 0..6 {
        if pass[i] < limit[i] {
            return true;
        } else if pass[i] > limit[i] {
            return false;
        }
    }

    return false;
}

fn exactly_two(pass: &Vec<u8>) -> bool {
    let mut twos = HashSet::new();
    let mut threes = HashSet::new();
    for i in 0..5 {
        if pass[i] == pass[i + 1] {
            twos.insert(pass[i]);
        }
    }
    for i in 0..4 {
        if pass[i] == pass[i + 1] && pass[i + 1] == pass[i + 2] {
            threes.insert(pass[i]);
        }
    }
    twos.len() > threes.len()
}

fn main() {
    let start = vec![1, 4, 5, 8, 5, 2];
    let end = vec![6, 1, 6, 9, 4, 2];
    // Solve part 1
    let mut pass = start.clone();
    let mut canidates = Vec::new();
    while lt(&pass, &end) {
        if valid_password(&pass) {
            canidates.push(pass.clone());
        }
        pass = next_password(pass);
    }

    println!("Part 1 - Count: {}", canidates.len());

    // Solve part 2
    let nt = canidates
        .into_iter()
        .filter(|x| exactly_two(x))
        .collect::<Vec<_>>();
    println!("Part 2 - Count: {}", nt.len());
}
