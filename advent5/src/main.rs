use std::cell::{Ref, RefCell};
use std::collections::VecDeque;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{self, prelude::*, BufReader};
use std::rc::Rc;

struct GraphNode {
    name: String,
    parents: HashMap<String, Rc<RefCell<GraphNode>>>,
    children: HashMap<String, Rc<RefCell<GraphNode>>>,
}

impl GraphNode {
    fn new(name: &str) -> GraphNode {
        GraphNode {
            name: name.to_string(),
            parents: HashMap::new(),
            children: HashMap::new(),
        }
    }

    fn add_parent(&mut self, parent: Rc<RefCell<GraphNode>>) {
        let name = parent.borrow().name.clone();
        self.parents.insert(name, parent);
    }

    fn add_child(&mut self, child: Rc<RefCell<GraphNode>>) {
        let name = child.borrow().name.clone();
        self.children.insert(name, child);
    }

    fn count_parents(&self) -> usize {
        self.parents.len()
    }

    fn count_children(&self) -> usize {
        self.children.len()
    }

    fn all_children(&self) -> usize {
        let mut total = self.children.len();
        for (_, node) in &self.children {
            let child = node.borrow();
            total += child.all_children();
        }
        total
    }

    fn print(&self, indentation: usize) {
        let mut ident = String::new();
        for _ in 0..indentation {
            ident.push(' ');
        }
        println!("{}-{}:", ident, self.name);
        for (_, node) in &self.children {
            let child = node.borrow();
            child.print(indentation + 2);
        }
    }

    fn direct_orbits(&self) -> usize {
        let mut orbits = self.count_children();
        for (_, node) in &self.children {
            let child = node.borrow();
            orbits += child.direct_orbits();
        }

        orbits
    }

    fn indirect_orbits(&self) -> usize {
        let mut total = 0;
        for (_, node) in &self.children {
            let child = node.borrow();
            // Add this child's children to the total
            total += child.all_children();
            // Recurse
            total += child.indirect_orbits();
        }

        total
    }

    fn has_child(&self, name: &str) -> Option<usize> {
        if self.children.contains_key(name) {
            Some(0)
        } else if self.children.is_empty() {
            None
        } else {
            let mut min = usize::max_value();
            let mut found = false;
            for (_, n) in &self.children {
                let child = n.borrow();
                if let Some(n) = child.has_child(name) {
                    if n + 1 < min {
                        found = true;
                        min = n + 1;
                    }
                }
            }

            if found {
                Some(min)
            } else {
                None
            }
        }
    }
}

struct Graph {
    index: HashMap<String, Rc<RefCell<GraphNode>>>,
}

impl Graph {
    fn new() -> Graph {
        Graph {
            index: HashMap::new(),
        }
    }

    fn link(&mut self, parent: &str, child: &str) {
        // Find the parent, making it does not exist
        let parent = if self.index.contains_key(parent) {
            self.index.get(parent).unwrap().clone()
        } else {
            let tmp = Rc::new(RefCell::new(GraphNode::new(parent)));
            self.index.insert(parent.to_string(), tmp.clone());
            tmp
        };

        // Find the child, making it if it does not exist
        let child = if self.index.contains_key(child) {
            self.index.get(child).unwrap().clone()
        } else {
            let tmp = Rc::new(RefCell::new(GraphNode::new(child)));
            self.index.insert(child.to_string(), tmp.clone());
            tmp
        };

        // link the parent to the child
        parent.borrow_mut().add_child(child.clone());
        // link the child to the parent
        child.borrow_mut().add_parent(parent.clone());
    }

    fn get_node(&self, name: &str) -> Option<Ref<GraphNode>> {
        Some(self.index.get(name)?.borrow())
    }

    fn get_roots(&self) -> Vec<String> {
        let mut names = Vec::new();
        for (k, v) in &self.index {
            if v.borrow().count_parents() == 0 {
                names.push(k.to_string());
            }
        }
        names
    }

    fn print(&self) {
        for root in self.get_roots() {
            let node = self.get_node(&root).unwrap();
            node.print(0);
        }
    }

    fn shortest_path(&self, root: &str, name1: &str, name2: &str) -> Option<usize> {
        let mut root_node = self.get_node(root)?;
        let right = root_node.has_child(name1)?;
        let left = root_node.has_child(name2)?;
        let mut min = right + left;
        let mut min_node = None;
        for (_, n) in &root_node.children {
            let child = n.borrow();
            if let Some(depth1) = child.has_child(name1) {
                if let Some(depth2) = child.has_child(name2) {
                    if depth1 + depth2 < min {
                        min_node = Some(child.name.clone());
                        min = depth1 + depth2;
                    }
                }
            }
        }

        if let Some(node) = min_node {
            self.shortest_path(&node, name1, name2)
        } else {
            Some(right + left)
        }
    }
}

fn main() -> io::Result<()> {
    let mut graph = Graph::new();

    let file = File::open("../../input")?;
    let reader = BufReader::new(file);
    for line in reader.lines() {
        let line = line?;
        let parts = line.trim().split(')').collect::<Vec<_>>();
        graph.link(parts[0], parts[1]);
    }

    let root = &graph.get_roots()[0];
    //println!("Graph has {} roots.", graph.get_roots().len());
    //println!("Center of mass: {}", root);
    // graph.print();
    let root_node = graph.get_node(root).unwrap();

    let direct = root_node.direct_orbits();
    //println!("Direct orbits: {}", direct);
    let indirect = root_node.indirect_orbits();
    //println!("Indirect orbits: {}", indirect);

    println!("Part 1 - Total orbits: {}", direct + indirect);

    let transfers = graph.shortest_path(root, "YOU", "SAN");
    println!("Part 2 - Number of transfers: {}", transfers.unwrap());

    Ok(())
}
