use std::fs::read_to_string;

fn main() {
    let height = 6;
    let width = 25;

    let data_string = read_to_string("../../input").unwrap();
    let digits = data_string
        .trim()
        .chars()
        .map(|x| x.to_digit(10).unwrap())
        .collect::<Vec<_>>();
    let layers = digits
        .as_slice()
        .chunks_exact(height * width)
        .collect::<Vec<_>>();
    println!("Number of layers: {}", layers.len());

    // Part 1
    let mut checksum = 0;
    let mut min_zeros = u32::max_value();
    for layer in &layers {
        let zeros = count_occurances(layer, 0);
        if zeros < min_zeros {
            min_zeros = zeros;
            checksum = count_occurances(layer, 1) * count_occurances(layer, 2);
        }
    }

    println!("Part 1 - {}", checksum);

    // Part 2
    let mut stack = vec![2; layers[0].len()];
    for layer in layers {
        for (i, v) in layer.iter().enumerate() {
            if stack[i] == 2 {
                stack[i] = *v;
            }
        }
    }

    let image = stack.chunks_exact(width).collect::<Vec<_>>();
    for row in image {
        for d in row {
            if *d == 0 {
                print!("___");
            } else {
                print!("_0_");
            }
        }
        println!();
    }
}

fn count_occurances(layer: &[u32], value: u32) -> u32 {
    let mut count = 0;
    for v in layer {
        if *v == value {
            count += 1;
        }
    }
    count
}
