use intcode_rs::*;

fn main() {
    // Part 1
    // Load in BOOST program
    let mut program = IntCode::from_file("../../day9");
    // Give it the self test input
    program.step_until_error();
    program.provide_input(1).unwrap();
    program.step_until_error();
    // Print the outputs
    println!("Part 1");
    let output = program.get_output();
    for o in output {
        println!("{}", o);
    }
    println!();

    // Part 2
    // Load in BOOST program
    let mut program = IntCode::from_file("../../day9");
    // Give it the self test input
    program.step_until_error();
    program.provide_input(2).unwrap();
    program.step_until_error();
    // Print the outputs
    println!("Part 2");
    let output = program.get_output();
    for o in output {
        println!("{}", o);
    }
}
