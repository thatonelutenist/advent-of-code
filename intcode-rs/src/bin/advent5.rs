use intcode_rs::*;

fn main() {
    // Solve part 1
    let mut intcode = IntCode::from_file("../../day5");
    let state = intcode.step_until_error();
    if let Failure::AwaitingInput(_) = state {
        intcode.provide_input(1).unwrap();
        let state = intcode.step_until_error();
        if let Failure::Halted = state {
            let output = intcode.get_output();
            println!("Part 1 - output: {}", output[output.len() - 1]);
        } else {
            panic!("Did not halt cleanly.");
        }
    } else {
        panic!("Did not request input");
    }

    // Solve Part 2
    let mut intcode = IntCode::from_file("../../day5");
    let state = intcode.step_until_error();
    if let Failure::AwaitingInput(_) = state {
        intcode.provide_input(5).unwrap();
        let state = intcode.step_until_error();
        if let Failure::Halted = state {
            let output = intcode.get_output();
            println!("Part 2 - output: {}", output[output.len() - 1]);
        } else {
            println!("{:?}", intcode);
            let memory = intcode.into_vec();
            println!("Memory Size: {}", memory.len());
            panic!("Did not halt cleanly");
        }
    } else {
        panic!("Did not request input");
    }

    // Solve Part 2
}
