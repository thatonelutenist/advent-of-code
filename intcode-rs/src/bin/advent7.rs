use intcode_rs::*;
use std::collections::HashSet;

fn run(sequence: &[isize], program: &IntCode) -> isize {
    let mut input = 0;
    for value in sequence {
        let mut intcode = program.clone();
        // provide the amplifer its phase setting
        intcode.step_until_error();
        intcode.provide_input(*value).unwrap();
        intcode.step_until_error();
        // Provide the amplifer its input
        intcode.provide_input(input).unwrap();
        intcode.step_until_error();
        // Collect its output
        input = intcode.get_output()[0];
    }

    input
}

fn run_feedback(sequence: &[isize], program: &IntCode) -> isize {
    let mut amps = Vec::new();
    // Create each amplifier and input its phase setting
    for i in 0..sequence.len() {
        let mut amp = program.clone();
        amp.step_until_error();
        amp.provide_input(sequence[i]).unwrap();
        amp.step_until_error();
        amps.push(amp)
    }

    let mut input = 0;
    let mut halted = false;
    while !halted {
        let mut count = 0;
        for amp in &mut amps {
            let state = amp.get_state();
            if state == Failure::Halted {
                count += 1;
            } else {
                amp.provide_input(input).unwrap();
                amp.step_until_error();
                let output = amp.get_output();
                input = output[output.len() - 1];
            }
        }
        if count == amps.len() {
            halted = true;
        }
    }

    let output = amps[amps.len() - 1].get_output();
    output[output.len() - 1]
}

fn generate_no_repeats(choices: HashSet<isize>, length: usize) -> Vec<Vec<isize>> {
    let mut output = Vec::new();
    if length == 1 {
        for i in choices {
            output.push(vec![i]);
        }
    } else {
        for d in &choices {
            let mut new_choices = choices.clone();
            new_choices.remove(d);
            let mut seqs = generate_no_repeats(new_choices, length - 1);
            for seq in &mut seqs {
                let mut new_vec = vec![*d];
                new_vec.append(seq);
                output.push(new_vec);
            }
        }
    }

    output
}

fn main() {
    // Solve Part 1
    let len = 5;
    let combos = generate_no_repeats((0..len).collect::<HashSet<isize>>(), len as usize);
    let intcode = IntCode::from_file("../../day7");
    let mut max_value = isize::min_value();
    for combo in combos {
        let value = run(&combo[..], &intcode);
        if value > max_value {
            max_value = value;
        }
    }

    println!("Part 1 - {}", max_value);

    // Solve Part 2
    let len = 5;
    let combos = generate_no_repeats((5..len + 5).collect::<HashSet<isize>>(), len as usize);
    let intcode = IntCode::from_file("../../day7");
    let mut max_value = isize::min_value();
    for combo in combos {
        let value = run_feedback(&combo[..], &intcode);
        if value > max_value {
            max_value = value;
        }
    }

    println!("Part 2 - {}", max_value);
}
