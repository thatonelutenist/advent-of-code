#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Mode {
    Position(isize),
    Immediate(isize),
    Relative(isize),
}

impl Mode {
    pub fn get_value(&self, intcode: &mut IntCode) -> Result<isize, Failure> {
        match self {
            Self::Position(x) => {
                if *x < 0 {
                    Err(Failure::OutOfBoundsAccess)
                } else if *x as usize >= intcode.memory.len() {
                    intcode.grow_memory(*x);
                    Ok(intcode.memory[*x as usize])
                } else {
                    Ok(intcode.memory[*x as usize])
                }
            }
            Self::Immediate(x) => Ok(*x),
            Self::Relative(y) => {
                let x = *y + intcode.rel_base;
                if x < 0 {
                    Err(Failure::OutOfBoundsAccess)
                } else if x as usize >= intcode.memory.len() {
                    intcode.grow_memory(x);
                    Ok(intcode.memory[x as usize])
                } else {
                    Ok(intcode.memory[x as usize])
                }
            }
        }
    }

    pub fn get_pointer(&self, intcode: &IntCode) -> isize {
        match self {
            Self::Position(x) => *x,
            Self::Immediate(x) => *x,
            Self::Relative(x) => *x + intcode.rel_base,
        }
    }

    pub fn from_int(mode: isize, value: isize) -> Result<Mode, Failure> {
        match mode {
            0 => Ok(Self::Position(value)),
            1 => Ok(Self::Immediate(value)),
            2 => Ok(Self::Relative(value)),
            _ => Err(Failure::InvalidMode),
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum OpCode {
    Add(Mode, Mode, Mode),
    Mul(Mode, Mode, Mode),
    Input(Mode),
    Output(Mode),
    JumpIfTrue(Mode, Mode),
    JumpIfFalse(Mode, Mode),
    LessThan(Mode, Mode, Mode),
    Equals(Mode, Mode, Mode),
    SetRelBase(Mode),
    Halt,
}

impl OpCode {
    pub fn advance_pointer(&self) -> usize {
        match self {
            Self::Add(..) => 4,
            Self::Mul(..) => 4,
            Self::Input(..) => 2,
            Self::Output(..) => 2,
            Self::JumpIfFalse(..) => 0,
            Self::JumpIfTrue(..) => 0,
            Self::LessThan(..) => 4,
            Self::Equals(..) => 4,
            Self::SetRelBase(..) => 2,
            Self::Halt => 0,
        }
    }
    pub fn from_slice(slice: &[isize]) -> Result<OpCode, Failure> {
        if slice.is_empty() {
            return Err(Failure::OutOfBoundsAccess);
        }
        let modenums = (slice[0] / 100) as isize;
        match slice[0] % 100 {
            1 => {
                if slice.len() < 4 {
                    Err(Failure::OutOfBoundsAccess)
                } else {
                    let mode1 = Mode::from_int(modenums % 10, slice[1])?;
                    let mode2 = Mode::from_int((modenums / 10) % 10, slice[2])?;
                    let mode3 = Mode::from_int((modenums / 100) % 10, slice[3])?;
                    Ok(OpCode::Add(mode1, mode2, mode3))
                }
            }
            2 => {
                if slice.len() < 4 {
                    Err(Failure::OutOfBoundsAccess)
                } else {
                    let mode1 = Mode::from_int(modenums % 10, slice[1])?;
                    let mode2 = Mode::from_int((modenums / 10) % 10, slice[2])?;
                    let mode3 = Mode::from_int((modenums / 100) % 10, slice[3])?;
                    Ok(OpCode::Mul(mode1, mode2, mode3))
                }
            }
            3 => {
                if slice.len() < 2 {
                    Err(Failure::OutOfBoundsAccess)
                } else {
                    let mode = Mode::from_int(modenums % 10, slice[1])?;
                    Ok(OpCode::Input(mode))
                }
            }
            4 => {
                if slice.len() < 2 {
                    Err(Failure::OutOfBoundsAccess)
                } else {
                    let mode = Mode::from_int(modenums % 10, slice[1])?;
                    Ok(OpCode::Output(mode))
                }
            }
            5 => {
                if slice.len() < 3 {
                    Err(Failure::OutOfBoundsAccess)
                } else {
                    let mode1 = Mode::from_int(modenums % 10, slice[1])?;
                    let mode2 = Mode::from_int((modenums / 10) % 10, slice[2])?;
                    Ok(OpCode::JumpIfTrue(mode1, mode2))
                }
            }
            6 => {
                if slice.len() < 3 {
                    Err(Failure::OutOfBoundsAccess)
                } else {
                    let mode1 = Mode::from_int(modenums % 10, slice[1])?;
                    let mode2 = Mode::from_int((modenums / 10) % 10, slice[2])?;
                    Ok(OpCode::JumpIfFalse(mode1, mode2))
                }
            }
            7 => {
                if slice.len() < 4 {
                    Err(Failure::OutOfBoundsAccess)
                } else {
                    let mode1 = Mode::from_int(modenums % 10, slice[1])?;
                    let mode2 = Mode::from_int((modenums / 10) % 10, slice[2])?;
                    let mode3 = Mode::from_int((modenums / 100) % 10, slice[3])?;
                    Ok(OpCode::LessThan(mode1, mode2, mode3))
                }
            }
            8 => {
                if slice.len() < 4 {
                    Err(Failure::OutOfBoundsAccess)
                } else {
                    let mode1 = Mode::from_int(modenums % 10, slice[1])?;
                    let mode2 = Mode::from_int((modenums / 10) % 10, slice[2])?;
                    let mode3 = Mode::from_int((modenums / 100) % 10, slice[3])?;
                    Ok(OpCode::Equals(mode1, mode2, mode3))
                }
            }
            9 => {
                if slice.len() < 2 {
                    Err(Failure::OutOfBoundsAccess)
                } else {
                    let mode1 = Mode::from_int(modenums % 10, slice[1])?;
                    Ok(OpCode::SetRelBase(mode1))
                }
            }
            99 => Ok(OpCode::Halt),
            _ => Err(Failure::IllegalInstruction),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Failure {
    Ok,
    Halted,
    IllegalInstruction,
    OutOfBoundsAccess,
    AwaitingInput(Mode),
    InvalidMode,
}

#[derive(Clone, Debug)]
pub struct IntCode {
    memory: Vec<isize>,
    pointer: usize,
    state: Failure,
    output: Vec<isize>,
    rel_base: isize,
}

impl IntCode {
    pub fn from_vec(vec: Vec<isize>) -> IntCode {
        IntCode {
            memory: vec,
            pointer: 0,
            state: Failure::Ok,
            output: Vec::new(),
            rel_base: 0,
        }
    }

    pub fn into_vec(self) -> Vec<isize> {
        self.memory
    }

    pub fn get_opcode(&mut self) -> Result<OpCode, Failure> {
        let opcode = OpCode::from_slice(&self.memory[self.pointer..])?;
        match opcode {
            OpCode::Add(_in1, _in2, out) | OpCode::Mul(_in1, _in2, out) => {
                let out = out.get_pointer(self);
                if out < 0 {
                    Err(Failure::OutOfBoundsAccess)
                } else if out as usize >= self.memory.len() {
                    self.grow_memory(out);
                    Ok(opcode)
                } else {
                    Ok(opcode)
                }
            }
            OpCode::JumpIfTrue(..) | OpCode::JumpIfFalse(..) | OpCode::SetRelBase(..) => Ok(opcode),
            OpCode::Output(_pointer) | OpCode::Input(_pointer) => Ok(opcode),
            OpCode::Equals(_m1, _m2, pointer) | OpCode::LessThan(_m1, _m2, pointer) => {
                let pointer = pointer.get_pointer(self);
                if pointer < 0 {
                    Err(Failure::OutOfBoundsAccess)
                } else if pointer as usize >= self.memory.len() {
                    self.grow_memory(pointer + 1);
                    Ok(opcode)
                } else {
                    Ok(opcode)
                }
            }
            OpCode::Halt => Ok(opcode),
        }
    }

    pub fn grow_memory(&mut self, new_max: isize) {
        if new_max > 0 {
            while new_max as usize >= self.memory.len() {
                self.memory.push(0);
            }
        }
    }

    pub fn step(&mut self) -> Result<(), Failure> {
        //println!("Pointer is at: {}", self.pointer);
        //let slicemax = if self.pointer + 5 >= self.memory.len() {
        //    self.memory.len() - 1
        //} else {
        //    self.pointer + 5
        //};
        //println!("Relative Base is: {}", self.rel_base);
        //println!("Memory length is: {}", self.memory.len());
        //println!(
        //    "Next 5 bytes are: {:?}",
        //    &self.memory[self.pointer..slicemax]
        //);
        let opcode = self.get_opcode()?;
        //println!("Opcode is: {:?}", opcode);

        self.pointer += opcode.advance_pointer();
        match opcode {
            OpCode::Add(in1, in2, out) => {
                let out = out.get_pointer(self);
                self.memory[out as usize] = in1.get_value(self)? + in2.get_value(self)?;
                Ok(())
            }
            OpCode::Mul(in1, in2, out) => {
                let out = out.get_pointer(self);
                self.memory[out as usize] = in1.get_value(self)? * in2.get_value(self)?;
                Ok(())
            }
            OpCode::Input(out) => Err(Failure::AwaitingInput(out)),
            OpCode::Output(pointer) => {
                let output = pointer.get_value(self)?;
                self.output.push(output);
                Ok(())
            }
            OpCode::JumpIfTrue(input, output) => {
                let input = input.get_value(self)?;
                let output = output.get_value(self)?;
                if input != 0 {
                    if output < 0 {
                        Err(Failure::OutOfBoundsAccess)
                    } else if output as usize >= self.memory.len() {
                        self.grow_memory(output);
                        self.pointer = output as usize;
                        Ok(())
                    } else {
                        self.pointer = output as usize;
                        Ok(())
                    }
                } else {
                    self.pointer += 3;
                    Ok(())
                }
            }
            OpCode::JumpIfFalse(input, output) => {
                let input = input.get_value(self)?;
                let output = output.get_value(self)?;
                if input == 0 {
                    if output < 0 {
                        Err(Failure::OutOfBoundsAccess)
                    } else if output as usize >= self.memory.len() {
                        self.grow_memory(output);
                        self.pointer = output as usize;
                        Ok(())
                    } else {
                        self.pointer = output as usize;
                        Ok(())
                    }
                } else {
                    self.pointer += 3;
                    Ok(())
                }
            }
            OpCode::LessThan(input1, input2, output) => {
                let output = output.get_pointer(self);
                let input1 = input1.get_value(self)?;
                let input2 = input2.get_value(self)?;
                if input1 < input2 {
                    self.memory[output as usize] = 1;
                } else {
                    self.memory[output as usize] = 0;
                }
                Ok(())
            }
            OpCode::Equals(input1, input2, output) => {
                let output = output.get_pointer(self);
                let input1 = input1.get_value(self)?;
                let input2 = input2.get_value(self)?;
                if input1 == input2 {
                    self.memory[output as usize] = 1;
                } else {
                    self.memory[output as usize] = 0;
                }
                Ok(())
            }
            OpCode::SetRelBase(input) => {
                let input = input.get_value(self)?;
                self.rel_base += input;
                Ok(())
            }
            OpCode::Halt => Err(Failure::Halted),
        }
    }

    pub fn step_until_error(&mut self) -> Failure {
        let mut result = self.step();
        while result.is_ok() {
            result = self.step();
        }

        self.state = result.unwrap_err();
        self.state
    }

    pub fn set_memory(&mut self, pointer: usize, value: isize) {
        self.memory[pointer] = value;
    }

    pub fn get_memory(&self, pointer: usize) -> isize {
        self.memory[pointer]
    }

    pub fn provide_input(&mut self, input: isize) -> Result<(), Failure> {
        if let Failure::AwaitingInput(pointer) = self.state {
            match pointer {
                Mode::Position(x) => {
                    if x < 0 {
                        Err(Failure::OutOfBoundsAccess)
                    } else if x as usize >= self.memory.len() {
                        self.grow_memory(x);
                        self.memory[x as usize] = input;
                        Ok(())
                    } else {
                        self.memory[x as usize] = input;
                        Ok(())
                    }
                }
                Mode::Immediate(_) => unimplemented!(),
                Mode::Relative(y) => {
                    let x = y + self.rel_base;
                    if x < 0 {
                        Err(Failure::OutOfBoundsAccess)
                    } else {
                        self.memory[x as usize] = input;
                        Ok(())
                    }
                }
            }
        } else {
            Err(self.state)
        }
    }

    pub fn get_output(&self) -> &Vec<isize> {
        &self.output
    }

    pub fn from_file(path: &str) -> IntCode {
        let string = std::fs::read_to_string(path).unwrap();
        let mut memory = Vec::new();
        for token in string.trim().split(',') {
            memory.push(token.parse::<isize>().unwrap());
        }

        IntCode::from_vec(memory)
    }

    pub fn get_state(&self) -> Failure {
        self.state
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn advent_2_1() {
        let input = vec![1, 0, 0, 0, 99];
        let mut intcode = IntCode::from_vec(input);
        let result = intcode.step_until_error();

        assert_eq!(result, Failure::Halted);
        assert_eq!(intcode.into_vec(), vec![2, 0, 0, 0, 99]);
    }
    #[test]
    fn advent_2_2() {
        let input = vec![2, 3, 0, 3, 99];
        let mut intcode = IntCode::from_vec(input);
        let result = intcode.step_until_error();

        assert_eq!(result, Failure::Halted);
        assert_eq!(intcode.into_vec(), vec![2, 3, 0, 6, 99]);
    }
    #[test]
    fn advent_2_3() {
        let input = vec![2, 4, 4, 5, 99, 0];
        let mut intcode = IntCode::from_vec(input);
        let result = intcode.step_until_error();

        assert_eq!(result, Failure::Halted);
        assert_eq!(intcode.into_vec(), vec![2, 4, 4, 5, 99, 9801]);
    }
    #[test]
    fn advent_2_4() {
        let input = vec![1, 1, 1, 4, 99, 5, 6, 0, 99];
        let mut intcode = IntCode::from_vec(input);
        let result = intcode.step_until_error();

        assert_eq!(result, Failure::Halted);
        assert_eq!(intcode.into_vec(), vec![30, 1, 1, 4, 2, 5, 6, 0, 99]);
    }

    #[test]
    fn advent_5_1() {
        let program = vec![3, 0, 4, 0, 99];
        let mut intcode = IntCode::from_vec(program);
        let input = 69;

        let state = intcode.step_until_error();
        assert!(if let Failure::AwaitingInput(_) = state {
            true
        } else {
            false
        });

        intcode.provide_input(input).unwrap();
        let state = intcode.step_until_error();
        assert_eq!(state, Failure::Halted);

        assert_eq!(intcode.get_output(), &vec![input]);
    }
    #[test]
    fn advent_5_2() {
        let program = vec![1002, 4, 3, 4, 33];
        let mut intcode = IntCode::from_vec(program);
        let state = intcode.step_until_error();

        assert_eq!(state, Failure::Halted);
        assert_eq!(intcode.into_vec(), vec![1002, 4, 3, 4, 99]);
    }
    #[test]
    fn advent_5_3() {
        let intcode = IntCode::from_vec(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]);
        // Test input != 8 position mode
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(7).unwrap();
        test.step_until_error();
        assert_eq!(test.get_output()[0], 0);
        // Test input == 8
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(8).unwrap();
        test.step_until_error();
        assert_eq!(test.get_output()[0], 1);

        let intcode = IntCode::from_vec(vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]);
        // Test input > 8 position mode
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(9).unwrap();
        test.step_until_error();
        assert_eq!(test.get_output()[0], 0);
        // Test input < 8
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(7).unwrap();
        test.step_until_error();
        assert_eq!(test.get_output()[0], 1);

        let intcode = IntCode::from_vec(vec![3, 3, 1108, -1, 8, 3, 4, 3, 99]);
        // Test input != 8 immediate mode
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(7).unwrap();
        test.step_until_error();
        assert_eq!(test.get_output()[0], 0);
        // Test input == 8
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(8).unwrap();
        test.step_until_error();
        assert_eq!(test.get_output()[0], 1);

        let intcode = IntCode::from_vec(vec![3, 3, 1107, -1, 8, 3, 4, 3, 99]);
        // Test input > 8 immediate mode
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(9).unwrap();
        test.step_until_error();
        assert_eq!(test.get_output()[0], 0);
        // Test input < 8
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(7).unwrap();
        test.step_until_error();
        assert_eq!(test.get_output()[0], 1);
    }
    #[test]
    fn advent_5_4() {
        // Runs the provided jump tests (Position Mode)
        let intcode = IntCode::from_vec(vec![
            3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9,
        ]);

        // Input 0
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(0).unwrap();
        test.step_until_error();
        let output = test.get_output();
        println!("{:?}", test);
        assert_eq!(output[0], 0);
        // Input non zero
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(7).unwrap();
        test.step_until_error();
        let output = test.get_output();
        println!("{:?}", test);
        assert_eq!(output[0], 1);

        // Runs the provided jump tests (Immediate Mode)
        let intcode = IntCode::from_vec(vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1]);

        // Input 0
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(0).unwrap();
        test.step_until_error();
        let output = test.get_output();
        println!("{:?}", test);
        assert_eq!(output[0], 0);
        // Input non zero
        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(7).unwrap();
        test.step_until_error();
        let output = test.get_output();
        println!("{:?}", test);
        assert_eq!(output[0], 1);
    }
    #[test]
    fn advent_5_5() {
        // Uses the larger example to test if a number is greater than 8
        let intcode = IntCode::from_vec(vec![
            3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0,
            0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4,
            20, 1105, 1, 46, 98, 99,
        ]);

        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(7).unwrap();
        let state = test.step_until_error();
        println!("{:?}", state);
        assert_eq!(test.get_output()[0], 999);

        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(8).unwrap();
        let state = test.step_until_error();
        println!("{:?}", state);
        assert_eq!(test.get_output()[0], 1000);

        let mut test = intcode.clone();
        test.step_until_error();
        test.provide_input(9).unwrap();
        let state = test.step_until_error();
        println!("{:?}", state);
        assert_eq!(test.get_output()[0], 1001);
    }

    #[test]
    fn advent_9_1() {
        // Should output a copy of itself
        let program1 = vec![
            109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99,
        ];
        let mut intcode1 = IntCode::from_vec(program1.clone());
        let state = intcode1.step_until_error();
        let output = intcode1.get_output().clone();
        assert_eq!(state, Failure::Halted);
        assert_eq!(program1, output);

        // Should output a 16 digit number
        let mut intcode = IntCode::from_vec(vec![1102, 34915192, 34915192, 7, 4, 7, 99, 0]);
        let state = intcode.step_until_error();
        let output = intcode.get_output().clone();
        assert_eq!(state, Failure::Halted);
        assert!(output[0] > 1_000_000_000_000_000);

        // Should output large number in middle
        let mut intcode = IntCode::from_vec(vec![104, 1125899906842624, 99]);
        let state = intcode.step_until_error();
        let output = intcode.get_output().clone();
        assert_eq!(state, Failure::Halted);
        assert_eq!(output[0], 1125899906842624);
    }
}
